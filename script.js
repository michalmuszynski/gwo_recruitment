const contentContainer = document.querySelector('.content-container');
const contentList = document.querySelector('.data-list');
const fetchButton = document.querySelector('.fetch-button');
const greetContainer = document.querySelector('.greeting')
const fetchAddres = 'https://files.gwo.pl/custom/random-data.json';
const form = document.querySelector('.new-programmer-form');
const inputName = document.getElementById('programmer-name');
const inputFramework = document.getElementById('programmer-framework');
const inputExperience = document.getElementById('programmer-experience');
const inputAvailable = document.getElementById('availability');

form.addEventListener('submit', function(event) {
  event.preventDefault();
  const values = getValues();
  RecruitmentAgency.addProgrammer(values);
});

fetchButton.addEventListener('click', function() {
  RecruitmentAgency.getAllProgrammers();
})

inputExperience.addEventListener('focusout', () => validateIsANumber())

const RecruitmentAgency = 
  {
    programmers: [],
    addProgrammer(newProgrammer) {
      clearInputs()
      this.programmers = this.programmers.concat(newProgrammer)
      this.getShowCase(this.programmers)
    },
    deleteProgrammer(removedProgrammer) {
      this.programmers = this.programmers.filter(programmer => programmer.id !== removedProgrammer.id)
      this.getShowCase(this.programmers)
    },
    getAllProgrammers() {
      fetch(fetchAddres)
      .then(response => response.json())
      .then(data => {
        this.programmers = data.map((item, index) =>
        Object.assign({}, item, item.id = getUuid()))
      }).then(() => this.getShowCase(this.programmers))
    },
    getShowCase(data) {
      contentList.innerHTML = ''
      showGreeting()
      data.map(item => {
      showItemStructure(item)
    })
  }
}

window.onload = () => {
  RecruitmentAgency.getAllProgrammers()
}

const getUuid = ((x = 0) => () => x++)()

getValues = () => {
  const newProgrammer = {
    name: inputName.value,
    framework: inputFramework.value,
    experience: inputExperience.value,
    available: inputAvailable.value
  }
  return newProgrammer
}

validateIsANumber = () => {
  let inputValue = inputExperience.value;
  const checkIfNumber = (/[0-9]/).test(inputValue)
  !checkIfNumber ? showError() : true;
}

showError = () => {
  alert('Type a number, not a string!')
  inputExperience.value = '';
}

clearInputs = () => {
  const allInputs = Array.from(document.querySelectorAll('input'));
  allInputs.forEach(input => input.value = '')
}

showItemStructure = item => {
  const listItem = document.createElement('li');
  const removeButton = document.createElement('button');
  createRemoveButton(removeButton, item);
  const nameParagraph = document.createElement('p');
  nameParagraph.innerHTML = item.name;
  listItem.appendChild(nameParagraph);
  listItem.appendChild(removeButton);
  contentList.appendChild(listItem);
}

createRemoveButton = (removeButton, programmer) => {
  removeButton.classList.add('remove-button');
  removeButton.innerHTML = 'Remove';
  removeButton.addEventListener('click', () => RecruitmentAgency.deleteProgrammer(programmer));
}

showGreeting = () => {
  greetContainer.innerHTML = ''
  const greeting = document.createElement('h2');
  greeting.innerHTML = 'Hello! Here are the programmers names: ';
  greetContainer.append(greeting);
}
